# Docker + Jenkins
Test Docker containers in Jenkins

## Installation:

* `docker build -t docker-jenkins .`
* `docker run --name jenkins-data docker-jenkins echo "Jenkins Data Container"`
* `docker run -d --name jenkins -p 9090:8080 --volumes-from jenkins-data -v /var/run/docker.sock:/var/run/docker.sock docker-jenkins`
* open `http://localhost:9090`

## Usage
You will need to provide a `docker-compose` file in the repo you are testing to use for testing. An example of how this file might go together is as follows:

```yaml
app_test:
    build: .
    environment:
        ENV: testing
```

You will also need to provide a bash script for running the build step. An example of this file is as follows:

```bash
# Default compose args
COMPOSE_ARGS=" -f test.yml -p jenkins "
SERVICE="app_test"

# Make sure old containers are gone
sudo docker-compose $COMPOSE_ARGS stop
sudo docker-compose $COMPOSE_ARGS rm --force -v

# Build the system
sudo docker-compose $COMPOSE_ARGS build --no-cache

# Run the test suite
sudo docker-compose $COMPOSE_ARGS run $SERVICE

ERR=$?

if [ $ERR -eq 0 ]; then
    echo "Test Passed - Tagging"
    HASH=$(git rev-parse --short HEAD)
    sudo docker tag -f jenkins_app_test YOUR_CONTAINER_NAME:$HASH
    sudo docker tag -f jenkins_app_test YOUR_CONTAINER_NAME:newest
    
    echo "Pushing to Docker Hub"
    sudo docker login -e YOUR_REGISTRY_EMAIL -u YOUR_REGISTRY_USERNAME -p YOUR_REGISTRY_PASSWORD
    sudo docker push YOUR_CONTAINER_NAME:$HASH
    sudo docker push YOUR_CONTAINER_NAME:newest
fi

# Pull down the system
sudo docker-compose $COMPOSE_ARGS stop
sudo docker-compose $COMPOSE_ARGS rm --force -v

return $ERR
```

*note* The placeholders, `YOUR_CONTAINER_NAME`, `YOUR_REGISTRY_EMAIL`, `YOUR_REGISTRY_USERNAME`, and `YOUR_REGISTRY_PASSWORD` should be set properly, and can (should?) be extrapolated into environment variables which are configurable in Jenkins

*note* This expects that an testing environment is available in your Docker Compose setup.

A reference application that can be built with this can be found [here](https://bitbucket.org/vgtf/adult-swim-rsvp).
